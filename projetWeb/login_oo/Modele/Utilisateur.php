<?php

/**
 *
 */

class Utilisateur extends Modele {

	private $nomUser;
	//le constructeur
	public function __construct($pNomUser) {
		$this->nomUser = $pNomUser;
	}

	public function getUser() {
		return $this->nomUser;
	}
	//Methode pour inserer un user dans la base de donnees
	public function insertUser($UTILISATEUR, $MOTPASSE) {

		$sql = 'INSERT INTO UTILISATEUR (NOM_USER, MOTPASSE_USER) VALUES (?, ?)';

		$MOTPASSE_hache = password_hash($MOTPASSE, PASSWORD_DEFAULT);

		$this->executerRequete($sql, array($UTILISATEUR, $MOTPASSE_hache));
	}
	//Methode pour recuperer le mot de passe d'un user
	public function recupererMotDePasse($UTILISATEUR) {
		$sql      = 'select MOTPASSE_USER from UTILISATEUR where NOM_USER =?';
		$requette = $this->executerRequete($sql, array($UTILISATEUR));

		$resultat = $requette->fetch();

		$resultat = $resultat['MOTPASSE_USER'];

		return $resultat;
	}
	//Methode pour recuperer le mot de passe
	public function getMOTPASSE($nomUTILISATEUR) {
		$sql = 'select MOTPASSE_USER from UTILISATEUR where NOM_USER =?';

		$MOTPASSESupposeTrouve = $this->executerRequete($sql, array($nomUTILISATEUR));

		return $MOTPASSESupposeTrouve;
	}
	//Methode pour recuperer l'id d'un user
	public function getIdUser($nomUTILISATEUR) {
		$sql = 'select ID_USER from UTILISATEUR where NOM_USER =?';

		$requette = $this->executerRequete($sql, array($nomUTILISATEUR));

		$resultat = $requette->fetch();

		$idUser = $resultat['ID_USER'];

		return $idUser;
	}
	//Methode pour verifier si un user existe
	public function verificationSiLeUserExiste($UTILISATEUR) {

		$sql = 'select * from UTILISATEUR where NOM_USER =?';

		$UTILISATEURSupposeTrouve = $this->executerRequete($sql, array($UTILISATEUR));
		if ($UTILISATEURSupposeTrouve->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}
}

?>