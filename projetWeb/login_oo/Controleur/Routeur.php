<?php

//require 'Controleur/ControleurAcceuil.php';
//require 'Controleur/ControleurJournal.php';
//require 'Modele/Commentaire.php';
//require 'Modele/Utilisateur.php';

/*spl_autoload_register(function ($class_name) {
include 'Modele/'.$class_name.'.php';
});*/

/**
 *Le routeur de l'application
 */
class Routeur {

	private $ctrlAcceuil;
	private $ctrlJournal;
	//Le constructeur du routeur
	public function __construct() {
		$this->ctrlAcceuil = new ControleurAcceuil();
		$this->ctrlJournal = new ControleurJournal();
	}
	//Methode qui dirige les requetes
	public function routeurRequete() {
		try {
			$usernameEntre = "";
			$passwordEntre = "";

			//Conversion des balises html en textes
			if (isset($_POST['nom'])) {
				$usernameEntre = htmlspecialchars($_POST['nom']);
			}
			if (isset($_POST['mdp'])) {
				$passwordEntre = htmlspecialchars($_POST['mdp']);
				//$passwordEntre = password_hash($passwordEntre, PASSWORD_DEFAULT);
			}
			//gestion bouton deconnexion
			if (isset($_POST['deconn'])) {
				session_start();
				session_unset();
				session_destroy();
				$message = 'Vous etes deconnecte !';
				$this->ctrlAcceuil->gestionAcceuil($message);

				//gestion bouton supprimer commentaire
			} elseif (isset($_POST['supprimerCommentaire'])) {
				session_start();
				$this->ctrlJournal->gestionSuppression($_POST['supprimerCommentaire']);
				$message = "Message supprime avec succes";

				$this->ctrlJournal->gestionJournal($_SESSION['nom'], $message);

				//gestion bouton ecrire
			} elseif (isset($_POST['ecrire'])) {

				session_start();

				if (isset($_POST['comment']) and isset($_POST['ecrire'])) {

					$nouveauCommentaire = htmlspecialchars($_POST['comment']);

					$this->ctrlJournal->commenter($_SESSION['nom'], $nouveauCommentaire);
					$message = "Commentaire ajoute avec succes !";
				} else {
					$message = "Vous n\'avez ecrit aucun commentaire";
				}
				header('Location:/login_oo/index.php');

				//gestion bouton nouvelle utilisateur
			} elseif (isset($_POST['sub_nv'])) {

				$this->ctrlAcceuil->gestionNouvel($usernameEntre, $passwordEntre);

				//gestion bouton connexion
			} elseif (isset($_POST['sub_connexion'])) {

				$this->ctrlJournal->gestionConnexion($usernameEntre, $passwordEntre);

				//gestion enter dans la barre de navigation
			} elseif (!isset($_POST['sub_nv']) and !isset($_POST['sub_connexion'])) {

				session_start();

				if (isset($_SESSION['nom'])) {
					$this->ctrlJournal->gestionJournal($_SESSION['nom']);
				} else {
					$this->ctrlAcceuil->gestionAcceuil("");
				}
			}

		} catch (Exception $e) {
			$msgErreur = $e->getMessage();

			require 'Vue/vueErreur.php';
		}
	}
}

?>