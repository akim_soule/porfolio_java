      <?php

try {

	require 'Modele/accessBD.php';

	$usernameEntre = "";
	$passwordEntre = "";
	$message       = '';
	$message2      = '';
	$indicateur    = 0;
	if (!isset($_POST['sub_nv']) and !isset($_POST['sub_connexion'])) {
		$indicateur = 1;
		require 'Vue/vueAcceuil.php';

	} elseif (isset($_POST['nom'])) {
		$usernameEntre = htmlspecialchars($_POST['nom']);
	}
	if (isset($_POST['mdp'])) {
		$passwordEntre = htmlspecialchars($_POST['mdp']);
	}

	if (isset($_POST['sub_nv'])) {

		if (verificationSiLeUserExiste($usernameEntre)) {
			$message2 = 'Utilisateur deja existant</br>';
		} else {
			insertUser($usernameEntre, $passwordEntre);
			$message2 = 'Bienvenu dans la base de donnees '.$usernameEntre.'</br>';
		}
		$indicateur = 1;
		require 'Vue/vueAcceuil.php';
	} elseif (isset($_POST['sub_connexion'])) {

		if (verificationSiLeUserExiste($usernameEntre)) {
			$motDePasseDeLaBaseDeDonnee     = recupererMotDePasse($usernameEntre);
			$estCeQueLeMotDePasseEstCorrect = password_verify($passwordEntre, $motDePasseDeLaBaseDeDonnee);
			if ($estCeQueLeMotDePasseEstCorrect) {
				session_start();
				$_SESSION['nom'] = $usernameEntre;
				$_SESSION['pwd'] = $passwordEntre;
				require 'Vue/vueJournal.php';
			} else {
				throw new Exception("Mauvais identifiant ou mot de passe");
			}
		} else {
			throw new Exception("Mauvais identifiant ou mot de passe");
		}
	}
} catch (Exception $e) {
	$msgErreur = $e->getMessage();
	require 'Vue/vueErreur.php';

}
?>
