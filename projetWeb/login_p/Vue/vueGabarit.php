<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<link rel="stylesheet" href="Contenu/style.css" />
	<title><?=$titre ?></title>
</head>
<body>
	<div id="global">
		<header>
			<h1 id="titreBlog"><?=$titre ?></h1>
			<hr width="30%" color="black" size="1" style="float: left;"></br>
		</header>
		<!-- #contenu -->
		<div id="contenu" style="height: 200px;">
		<?=$contenu ?>
		</div>
		
		<footer id="piedBlog"> 
			<hr width="30%" color="black" size="1" style="float: left;"></br>	
			TP3 420-306-LI 2018 
		</footer>
	</div>
	<!-- #global -->
</body>
</html>

