<?php

function insertUser($UTILISATEUR, $MOTPASSE) {
	$bdd            = getBdd();
	$MOTPASSE_hache = password_hash($MOTPASSE, PASSWORD_DEFAULT);

	$req = $bdd->prepare('INSERT INTO UTILISATEUR (NOM, MOTPASSE) VALUES (:nom, :MOTPASSE)');
	$req->execute(array(
			'nom'      => $UTILISATEUR,
			'MOTPASSE' => $MOTPASSE_hache));
}

function recupererMotDePasse($UTILISATEUR) {
	$bdd = getBdd();

	$requette = $bdd->prepare('select MOTPASSE from UTILISATEUR where NOM = :nom');
	$requette->execute(array(
			'nom' => $UTILISATEUR));

	$resultat = $requette->fetch();

	$resultat = $resultat['MOTPASSE'];

	return $resultat;
}

function verificationSiLeUserExiste($UTILISATEUR) {

	$bdd                      = getBdd();
	$UTILISATEURSupposeTrouve = $bdd->prepare('select * from UTILISATEUR where NOM = :nom');
	$UTILISATEURSupposeTrouve->execute(array(
			'nom' => $UTILISATEUR));
	if ($UTILISATEURSupposeTrouve->rowCount() > 0) {
		return true;
	} else {
		return false;

	}
}

// Renvoie le mot de passe du UTILISATEUR
function getMOTPASSE($nomUTILISATEUR) {
	$bdd                   = getBdd();
	$MOTPASSESupposeTrouve = $bdd->prepare('select MOTPASSE from UTILISATEUR where NOM = :nom');
	$MOTPASSESupposeTrouve->execute(array(
			'nom' => $nomUTILISATEUR));

	return $MOTPASSESupposeTrouve;
}

// Effectue la connexion à la BDD
// Instancie et renvoie l'objet PDO associé
function getBdd() {
	$bdd = new PDO('mysql:host=localhost;dbname=tp3a18;charset=utf8', 'root',
		'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	return $bdd;
}

?>


