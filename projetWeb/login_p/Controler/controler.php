<?php 

require 'Modele/accessBD.php';

function gestionPageConnexion($pseudoEntre, $motDePasseEntre){

	if (isset($_POST['inscription'])){
        $message = '';
        $ok = 1;

        if (isset($pseudoEntre) and $pseudoEntre != ""){

        	if (verificationSiLeUserExiste($pseudoEntre)){
        		$ok = 0;
        		$message .= 'Utilisateur deja existant</br>';
        		
        	}else{

        		if (isset($motDePasseEntre) and $motDePasseEntre != ""){
        			
	        		insertUser($pseudoEntre, $motDePasseEntre);
	        		$message = 'Bienvenu dans la base de donnees'.$pseudoEntre.'</br>';
        		}else{
        			$ok = 0;
        			$message .= 'Vous n\'avez pas entre un mot de passe</br>';
        		}
        	}
        }else{
        	$message .= 'Vous n\'avez pas entre un pseudo</br>';
        	$ok = 0;
        }


        header('Location:vueConnexion.php?mes='.$message);
        
    }
    elseif (isset($_POST['connexion']))
    {
        if  (isset($pseudoEntre) and 
            	$pseudoEntre != "" and 

            isset($motDePasseEntre) and 
            $motDePasseEntre != "") {

            if (verificationSiLeUserExiste($pseudoEntre)){

            	$motDePasseDeLaBaseDeDonnee = recupererMotDePasse($pseudoEntre);
					
					$estCeQueLeMotDePasseEstCorrect = password_verify($motDePasseEntre, $motDePasseDeLaBaseDeDonnee);

					if ($motDePasseDeLaBaseDeDonnee){
						session_start();
						$_SESSION['pseudo'] = $pseudoEntre;
						header("Location: vueJournal.php?nom=".$_SESSION['pseudo']);
					}else{
						throw new Exception("Mauvais identifiant ou mot de passe");
					}

			}else{
				throw new Exception("Mauvais identifiant ou mot de passe");
			}
        }else{
        	throw new Exception("Vous n'avez entre aucun utilisateur ou mot de passe");
        	
        }
    }
}

 ?>