package utilitaires;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sylvain Tremblay
 * 
 */
public class ArbreBinaireDerive<T> implements Serializable {
	public NoeudArbreDerive<T> getRacine() {
		return racine;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * le premier noeud
	 */
	private NoeudArbreDerive<T> racine = null;

	/**
	 * Constructeur
	 */
	public ArbreBinaireDerive() {
		racine = null;
	}

	/**
	 * verifie si l'arbre est vide
	 * 
	 * @return boolean, vrai si l'arbre est vide
	 */
	public boolean arbreVide() {
		return racine == null;
	}

	/**
	 * Vide l'arbre
	 */
	public void vider() {
		racine = null;
		System.gc();
	}

	/**
	 * Methode interne utilitaire qui retourne la reference au noeud dont l'element
	 * contient obj.
	 * 
	 * @param actuel le noeud actuel
	 * @param obj    l'objet contenu dans le noeud recherché
	 * 
	 * @return NoeudArbreDerive, le noeud recherché, ou null si pas trouvé
	 */
	private NoeudArbreDerive<T> rechercherNoeud(NoeudArbreDerive<T> actuel, T obj) {
		NoeudArbreDerive<T> retour = null;

		// Si le noeud actuel n'est pas null
		if (actuel != null) {
			// Si le noeud actuel est le noeud recherché
			if (actuel.getElement().equals(obj)) {
				retour = actuel;
			} else {
				// Rechercher avec le noeud fils du noeud actuel
				retour = rechercherNoeud(actuel.getFils(), obj);

				// Rechercher avec le noeud frère du noeud actuel
				if (retour == null) {
					retour = rechercherNoeud(actuel.getFrere(), obj);
				}
			}
		}
		return retour;
	}

	/**
	 * Methode interne utilitaire qui retourne la reference au noeud dont l'attribut
	 * fils ou l'attribut frere qui refere a obj (en relite, au noeud qui contient
	 * obj); Par exemple, en cas de suppression d'un noeud, cette reference
	 * permettra a la methode supprimer de rattacher les noeuds restants a l'arbre
	 * 
	 * @param actuel Noeud actuel
	 * @param obj    l'objet contenu dans le noeud recherche
	 * 
	 * @return NoeudArbreDerive, un pointeur sur le noeud precedent si trouve, ou
	 *         null si pas trouve
	 */
	private NoeudArbreDerive<T> rechercherPrec(NoeudArbreDerive<T> actuel, T obj) {
		NoeudArbreDerive<T> retour = null;

		if (actuel != null) {
			// Si le fils du noeud actuel est le noeud contenant
			// l'objet recherché
			if ((actuel.getFils() != null) && (actuel.getFils().getElement().equals(obj))) {
				retour = actuel;
			} else {
				// Si le noeud frere du noeud actuel est le noeud
				// contenant l'objet recherche
				if (actuel.getFrere() != null && actuel.getFrere().getElement().equals(obj)) {
					retour = actuel;
				}
				// Si le noeud rechercher n'est ni le fils, ni le
				// frere du noeud actuel
				else {
					// Rechercher avec le fils du noeud actuel
					if (actuel.getFils() != null) {
						retour = rechercherPrec(actuel.getFils(), obj);
					}
					// Rechercher avec le noeud frere du noeud actuel
					if ((retour == null) && (actuel.getFrere() != null)) {
						retour = rechercherPrec(actuel.getFrere(), obj);
					}
				}
			}
		}

		return retour;
	}

	/**
	 * Methode interne utilitaire qui retourne la reference au parent du noeud
	 * contenant obj.
	 * 
	 * @param actuel le noeud actuel
	 * @param obj    l'objet du noeud rechercher
	 * 
	 * @return NoeudArbreDerive, un pointeur sur le noeud parent si trouve, sinon,
	 *         null
	 */
	private NoeudArbreDerive<T> rechercherParent(NoeudArbreDerive<T> actuel, T obj) {
		NoeudArbreDerive<T> parent = null;
		NoeudArbreDerive<T> temp = null;

		if (actuel != null) {
			// Boucle pour verifier si le noeud actuel est parent
			// du noeud contenant obj
			temp = actuel.getFils();
			while ((temp != null) && !temp.getElement().equals(obj)) {
				temp = temp.getFrere();
			}

			// Si l'element d'un des fils du noeud actuel
			// correspond a obj
			if ((temp != null) && temp.getElement().equals(obj)) {
				parent = actuel;
			} else {
				// Sinon, rechercher avec le noeud fils du noeud
				// actuel
				parent = rechercherParent(actuel.getFils(), obj);

				// Si le retour est null, rechercher avec le
				// frere du noeud actuel
				if (parent == null) {
					parent = rechercherParent(actuel.getFrere(), obj);
				}
			}
		}

		return parent;
	}

	/**
	 * Savoir si un noeud existe dans l'arbre
	 * 
	 * @param obj Objet recherche
	 * 
	 * @return boolean, vrai si l'element obj appartient a l'arbre, faux sinon
	 */
	public boolean existe(T obj) {
		// Vrai si la recherche retourne quelque chose, faux sinon
		return (rechercherNoeud(this.racine, obj) != null);
	}

	/**
	 * Sert a ajouter un noeud a l'arbre; ce nouveau noeud contiendra obj; son
	 * parent dans l'arbre est pParent mais obj n'est pas forcement le premier
	 * enfant de paramParent! Retourne
	 * 
	 * @param obj     objet a ajouter
	 * @param pParent objet parent ou le noeud doit etre ajoute
	 * 
	 * @return boolean, vrai si reussite et faux sinon.
	 */
	public boolean ajouter(T obj, T pParent) {
		boolean retour = false;
		if (pParent == null && racine == null) {
			this.racine = new NoeudArbreDerive<T>((T) obj);
			retour = true;
		} else {
			NoeudArbreDerive<T> noeudRecheche = rechercherNoeud(racine, pParent);
			NoeudArbreDerive<T> nouveauNoeud = new NoeudArbreDerive<T>(obj);
			if (noeudRecheche == null) {
				if (racine != null) {
					NoeudArbreDerive<T> frereCourant = racine.getFrere();
					if (frereCourant == null) {
						retour = racine.setFrere(nouveauNoeud);
					} else {
						NoeudArbreDerive<T> frerePrecedent = frereCourant;
						while (frereCourant != null) {
							frerePrecedent = frereCourant;
							frereCourant = frereCourant.getFrere();
						}
						retour= frerePrecedent.setFrere(nouveauNoeud);
					}
				}else {
					NoeudArbreDerive<T> noeudRacine = new NoeudArbreDerive<>(pParent);
					racine = noeudRacine;
					retour = noeudRacine.setFils(new NoeudArbreDerive<>(obj));
				}
			} else {
				if (noeudRecheche.getFils() == null) {
					retour = noeudRecheche.setFils(nouveauNoeud);
				} else {
					NoeudArbreDerive<T> frereCourant = noeudRecheche.getFils().getFrere();

					if (frereCourant == null) {
						retour = noeudRecheche.getFils().setFrere(nouveauNoeud);
					} else {
						NoeudArbreDerive<T> frerePrecedent = frereCourant;
						while (frereCourant != null) {
							frerePrecedent = frereCourant;
							frereCourant = frereCourant.getFrere();
						}
						retour = frerePrecedent.setFrere(nouveauNoeud);
					}
				}
			}

		}
		
		return retour;
	}

	/**
	 * Methode qui met a jour l'element du noeud recherche avec le nouvel element
	 * recu
	 * 
	 * @param actuel le noeud actuel
	 * @param obj    l'objet contenu dans le noeud recherche
	 * @param newObj le nouveau contenu du noeud recherche
	 * 
	 * @return si la mise a jour a reussit
	 */
	public boolean mettreAJour(NoeudArbreDerive<T> actuel, T obj, T newObj) {
		boolean reussite = false;

		// Si le noeud actuel n'est pas null
		if (actuel != null) {
			// Si le noeud actuel est le noeud recherche
			if (actuel.getElement().equals(obj)) {
				actuel.setElement(newObj);
				reussite = true;
			} else {
				// rechercher avec le noeud fils du noeud actuel
				reussite = mettreAJour(actuel.getFils(), obj, newObj);

				// rechercher avec le noeud frere du noeud actuel
				if (!reussite) {
					reussite = mettreAJour(actuel.getFrere(), obj, newObj);
				}
			}
		}
		return reussite;
	}

	/**
	 * Enlever de l'arbre le noeud contenant obj. S'il a des descendants, ils
	 * doivent etre aussi supprimes; pas ses freres.
	 * 
	 * @param obj Objet à supprimer
	 * 
	 * @return vrai si reussite et faux sinon.
	 */
	public boolean supprimer(T obj) {
		// Rechercher le noeud a supprimer
		NoeudArbreDerive<T> recherche = rechercherNoeud(this.racine, obj);
		NoeudArbreDerive<T> noeudPrec = null;
		boolean reussite = false;

		// Si le noeud a supprimer existe
		if (recherche != null) {
			// Rechercher le noeud precedent
			noeudPrec = rechercherPrec(this.racine, obj);

			// Si le noeud precedent est le pere du noeud à
			// supprimer
			if ((noeudPrec.getFils() != null) && recherche.getElement().equals(noeudPrec.getFils().getElement())) {
				// Prend le premier frere
				noeudPrec.setFils(recherche.getFrere());
			}

			// Si il est le frere du noeud a supprimer
			else {
				noeudPrec.setFrere(recherche.getFrere());
			}

			// La suppression est reussite
			reussite = true;
		}

		return reussite;
	}

	/**
	 * Retourne une chaine contenant le contenu de l'arbre
	 * 
	 * @return String le contenu de l'arbre
	 */
	public String toString() {
		String chaine = "";
		int niveau = 0; // le niveau permettra de gerer
						// l'indentation dans la chaine.
		
		chaine = assistantRecursif(this.racine, niveau);

		return chaine;
	}

	/**
	 * Methode recursive qui ajoute les elements de l'arbre a la chaine de
	 * caractaires ch retournee a la fin de la methode. Cette methode est appelee
	 * par la methode precedente. Cette methode utilise un parcours prefixe
	 * 
	 * @param noeud  le noeud actuel
	 * @param niveau le niveau du noeud actuel
	 * @return la chaine contenant tout les noeud en dessous du noeud actuel
	 */
	private String assistantRecursif(NoeudArbreDerive<T> noeud, int niveau) {
		String retour = "";
		String space = "";
		// if (noeud != racine)
		for (int i = 0; i < niveau; i++) {
			space += " ";
		}
		if (noeud != null) {
			
				retour += space + noeud.getElement().toString() + "\n";

			if (noeud.getFils() != null) {
				retour += assistantRecursif(noeud.getFils(), 5 + niveau);
			}
			if (noeud.getFrere() != null) {
				retour += assistantRecursif(noeud.getFrere(), niveau);
			}
		}
		return retour;
	}

	/**
	 * Appel du parcours recursif prefixe
	 * 
	 */
	public String parcoursPrefixe() {
		return prefixe(this.racine);
	}

	/**
	 * Appel du parcours recursif infixe
	 * 
	 */
	public String parcoursInfixe() {
		return infixe(this.racine);
	}

	/**
	 * Appel du parcours recursif suffixe
	 * 
	 */
	public String parcoursSuffixe() {
		return suffixe(this.racine);
	}

	/**
	 * Methode qui retourne une lite du String des elements de l'arbre Elle est
	 * recursive.
	 * 
	 * @return la liste des elements de l'arbre en string
	 */
	public List<String> getListeElement() {
		List<String> list = new ArrayList<>();
		// si l'arbre n'est pas vide
		if (racine != null)
			list.add(racine.getElement().toString());

		// si la le fils de la racine n'est pas nul on recupere son arbre (pour le fils)
		// et on lui applique getListeElement()
		if (racine.getFils() != null)
			list.addAll(getSousArbre(racine.getFils().getElement()).getListeElement());

		// si la le frere de la racine n'est pas nul on recupere son arbre (pour le
		// frere)
		// et on lui applique getListeElement()
		if (racine.getFrere() != null)
			list.addAll(getSousArbre(racine.getFrere().getElement()).getListeElement());
		
		return list;

	}
	
	public int hauteurArbre() {
		int i = 1;
		int gaucheInter = 0;
		int droiteInter = 0;
		if (arbreVide()) {
			return 0;
		}else {
			if (racine.getFils() != null)
				gaucheInter += getSousArbre(racine.getFils().getElement()).hauteurArbre();
			if (racine.getFrere() != null)
				droiteInter += getSousArbre(racine.getFrere().getElement()).hauteurArbre();
			
			return (i + Math.max(gaucheInter, droiteInter));
		}
	}
	
	public int nombreDeNoeud() {
	return calcul(this.racine);
	}
	private int calcul(NoeudArbreDerive<T> n) {
		if (n==null)
		return 0;
		else 
			return (1+calcul(n.getFils())+calcul(n.getFrere()));
	}
	
	public int nombreDeNoeudSArbre() {
		int nombre = 1;
		if (arbreVide()) {
			return 0;
		}else {
			if (racine.getFils() != null)
				nombre += getSousArbreNoeud(this.racine.getFils()).nombreDeNoeud();
			if (racine.getFrere() != null)
				nombre += getSousArbreNoeud(this.racine.getFrere()).nombreDeNoeud();
		}
		return nombre;
	}
	public int nombreDeFeuille() {
		int nombre = 0;
		if (!arbreVide()) {
			if (this.getRacine().estFeuille()) {
				++nombre;
			}
			if (racine.getFils() != null) {
				nombre += getSousArbreNoeud(this.racine.getFils()).nombreDeFeuille();
			}
			if (racine.getFrere() != null) {
				nombre += getSousArbreNoeud(this.racine.getFrere()).nombreDeFeuille();
			}
		}
		return nombre;
		
	}

	/**
	 * Methode qui retourne le sous-arbre du noeud dont l'objet est passe en parametre.
	 * 
	 * @param element est le noeud dont on veut recuperer le sous arbre.
	 * @return le sous arbre du noeud dont le parametre est element.
	 */
	private ArbreBinaireDerive<T> getSousArbre(T element) {
		// on cree un nouvel arbre "nouvelArbre"
		ArbreBinaireDerive<T> nouvelArbre = new ArbreBinaireDerive<T>();

		// on cherche le noeud dont l'element correspond au parametre element
		// sur l'arbre auquel on applique la methode getSousArbre a partir de la racine
		NoeudArbreDerive<T> noeudElem = rechercherNoeud(this.racine, element);

		// Puis on lui comme racine au nouvel arbre le noeud trouve.
		nouvelArbre.racine = noeudElem;
		return nouvelArbre;
	}
	
	/**
	 * Methode qui retourne le sous-arbre du noeud passe en parametre.
	 * @param noeud qui est le noeud dont on veut recuprer l'arbre.
	 * @return le sous arbre du noeud passe en parametre.
	 */
	private ArbreBinaireDerive<T> getSousArbreNoeud(NoeudArbreDerive<T> noeud) {
		// on cree un nouvel arbre "nouvelArbre"
		ArbreBinaireDerive<T> nouvelArbre = new ArbreBinaireDerive<T>();

		// Puis on lui comme racine au nouvel arbre le noeud trouve.
		nouvelArbre.racine = noeud;
		return nouvelArbre;
	}

	/**
	 * Methodes privees pour le parcours recursif prefixe
	 * 
	 */
	private String prefixe(NoeudArbreDerive<T> nCourant) {
		String retour = "";

		if (nCourant != null) {
			// System.out.println(identation + noeudCourant.getElement().toString());
			retour = nCourant.getElement().toString() + "\n";
			if (nCourant.getFils() != null) {

				retour += prefixe(nCourant.getFils());
			}
			if (nCourant.getFrere() != null) {

				retour += prefixe(nCourant.getFrere());
			}
		}
		return retour;

	}

	/**
	 * Methodes privees pour le parcours recursif infixe
	 * 
	 */
	private String infixe(NoeudArbreDerive<T> nCourant) {
		String retour = "";

		// System.out.println(identation + noeudCourant.getElement().toString());
		if (nCourant.getFils() != null) {

			retour += prefixe(nCourant.getFils());
		}
		retour += nCourant.getElement().toString() + "--";
		if (nCourant.getFrere() != null) {

			retour += prefixe(nCourant.getFrere());
		}

		return retour;

	}

	/**
	 * Methodes privees pour le parcours recursif suffixe
	 */
	private String suffixe(NoeudArbreDerive<T> nCourant) {
		String retour = "";

		if (nCourant != null) {
			// System.out.println(identation + noeudCourant.getElement().toString());
			if (nCourant.getFils() != null) {

				retour += prefixe(nCourant.getFils());
			}
			if (nCourant.getFrere() != null) {

				retour += prefixe(nCourant.getFrere());
			}
			retour += nCourant.getElement().toString() + "--";
		}
		return retour;
	}

	/**
	 * Tests unitaires
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		ArbreBinaireDerive<Object> arbre = new ArbreBinaireDerive<Object>();
//		ArrayList<?> l = new ArrayList<>();
//		l.add(1, null);
//		l.add('E', null);

		arbre.ajouter(new String("Lucie"), null);
		arbre.ajouter(new String("Christian"), null);
		arbre.ajouter(new String("Mathieu"), new String("Lucie"));
		arbre.ajouter(new String("Roxanne"), new String("Mathieu"));
		arbre.ajouter(new String("Kim"), new String("Mathieu"));
		arbre.ajouter(new String("Lyne"), new String("Lucie"));
		arbre.ajouter(new String("Maxime"), new String("Lyne"));
		arbre.ajouter(new String("Ludovic"), new String("Christian"));
		arbre.ajouter(new String("Julliette"), new String("Maxime"));
		arbre.ajouter(new String("Simon"), new String("Maxime"));
		arbre.ajouter(new String("Leo"), new String("Simon"));
		arbre.ajouter(new String("Victor"), new String("Roxanne"));
		
		System.out.println("le nombre de feuilles est "+arbre.nombreDeFeuille());
		System.out.println("La hauteur de l'arbre est "+arbre.hauteurArbre());
		System.out.println("Le nombre de noeud est "+arbre.nombreDeNoeud());
		System.out.println("Le nombre de noeud est "+arbre.nombreDeNoeudSArbre());
		
//		
//		Stream<String> stream = arbre.getListeElement().stream();
//		List <Personne> list = stream.
//				map(x -> new Personne(x, new Random().nextInt((70 - 10) + 1) + 10)).
//				collect(Collectors.toList());
//		System.out.println(list.toString());
		
		System.out.println(arbre.ajouter('T', null));
		arbre.ajouter(new Character('W'), null);
		arbre.ajouter(new Character('N'), null);
		arbre.ajouter(new Character('K'), null);
		arbre.ajouter(new Character('P'), null);
		arbre.ajouter(new Character('F'), new Character('W'));
		arbre.ajouter(new Character('G'), new Character('W'));
		arbre.ajouter(new Character('D'), new Character('F'));
		arbre.ajouter(new Character('S'), new Character('F'));
		arbre.ajouter(new Character('Z'), new Character('S'));
		arbre.ajouter(new Character('J'), new Character('P'));
		arbre.ajouter(new Character('M'), new Character('J'));
		arbre.ajouter(new Character('C'), new Character('M'));

		System.out.println(arbre);
		System.out.println("Parcours prefixe");
		System.out.println(arbre.parcoursPrefixe());
		System.out.println("Parcours infixe");
		System.out.println(arbre.parcoursInfixe());
		System.out.println("Parcours suffixe");
		System.out.println(arbre.parcoursSuffixe());
		System.out.println("Le sous arbre de S");
		System.out.println(arbre.getSousArbre('S'));
		
		System.out.println(arbre.rechercherPrec(arbre.racine, new Integer(4)));
		
		System.out.println("Ajouter element:");
		arbre.ajouter(new Integer(1), null);
		arbre.ajouter(new Integer(2), new Integer(1));
		arbre.ajouter(new Integer(3), new Integer(1));
		arbre.ajouter(new Integer(4), new Integer(2));
		arbre.ajouter(new Integer(5), new Integer(3));
		arbre.ajouter(new Integer(6), new Integer(2));
		arbre.ajouter(new Integer(10), new Integer(2));
		arbre.ajouter(new Integer(11), new Integer(2));
		arbre.ajouter(new Integer(7), new Integer(3));
		arbre.ajouter(new Integer(8), new Integer(4));
		arbre.ajouter(new Integer(9), new Integer(5));
		System.out.println(arbre);
		arbre.getListeElement();
		System.out.println("Supprimer element:");
		arbre.supprimer(new Integer(5));
		arbre.supprimer(new Integer(8));
		System.out.println(arbre);
		System.out.println("Rechercher parent:");
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(4)).getElement());
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(6)).getElement());
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(10)).getElement());
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(11)).getElement());
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(2)).getElement());
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(3)).getElement());
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(7)).getElement());
		System.out.println("Mettre a jour element:");
		System.out.println(arbre.mettreAJour(arbre.racine, new Integer(7), new Integer(9)));
		System.out.println(arbre);
		System.out.println(arbre.mettreAJour(arbre.racine, new Integer(7), new Integer(9)));
		System.out.println(arbre);

		System.out.println("Rechercher parent:");
		System.out.println(arbre.rechercherParent(arbre.racine, new Integer(9)).getElement());

	}
}
