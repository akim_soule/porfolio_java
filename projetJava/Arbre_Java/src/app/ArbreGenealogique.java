package app;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import utilitaires.ArbreBinaireDerive;
import utilitaires.Personne;

public class ArbreGenealogique implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArbreBinaireDerive<Object> arbre = new ArbreBinaireDerive<Object>();
	private Set<Personne> setPersonne = new HashSet<Personne>();

	public ArbreGenealogique() {

	}

	public void genererArbreGenealogique() {
		arbre.ajouter(new String("Lucie"), null);
		arbre.ajouter(new String("Christian"), null);
		arbre.ajouter(new String("Mathieu"), new String("Lucie"));
		arbre.ajouter(new String("Roxanne"), new String("Mathieu"));
		arbre.ajouter(new String("Kim"), new String("Mathieu"));
		arbre.ajouter(new String("Lyne"), new String("Lucie"));
		arbre.ajouter(new String("Maxime"), new String("Lyne"));
		arbre.ajouter(new String("Ludovic"), new String("Christian"));
		arbre.ajouter(new String("Julliette"), new String("Maxime"));
		arbre.ajouter(new String("Simon"), new String("Maxime"));
		arbre.ajouter(new String("Leo"), new String("Simon"));
		arbre.ajouter(new String("Victor"), new String("Roxanne"));
	}

	public void ajouterEnfant(String enfant, String parent) {
		arbre.ajouter(enfant, parent);
	}

	public List<String> getListeElement() {
		return arbre.getListeElement();
	}

	public Set<Personne> transformeSetPeronne() {
		this.setPersonne = this.getListeElement().stream()
				.map(x -> new Personne(x, new Random().nextInt((70 - 10) + 1) + 10)).collect(Collectors.toSet());
		return setPersonne;
	}

	public List<Personne> getListePersonne40ansEtMoins() {
		Stream<Personne> s = this.setPersonne.stream();
		return s.filter(x -> x.getAge() < 40).collect(Collectors.toList());
	}

	public void serialize() {
		try (BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(new File("sauvegarde/serial.ser")));
				ObjectOutputStream oos = new ObjectOutputStream(bos)) {
			oos.writeObject(arbre);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
			// e.getSuppressed();
		}
	}

	/**
	 * Un SuppressWarning parce que lors du cast de l'objet retObj en
	 * ArbreBinaireDerive<Object>, il n'y a aucune garanti que l'objet retObj soit
	 * un arbre d'objets.
	 */
	@SuppressWarnings("unchecked")
	public void deserialize() {
		Object retObj = null;
		try (BufferedInputStream bos = new BufferedInputStream(new FileInputStream(new File("sauvegarde/serial.ser")));
				ObjectInputStream ois = new ObjectInputStream(bos)) {
			retObj = ois.readObject();
			arbre = (ArbreBinaireDerive<Object>) retObj;
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			// e.getSuppressed();
		}
	}

	@Override
	public String toString() {
		return arbre.toString();
	}
}
